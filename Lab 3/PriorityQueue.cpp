#include "PriorityQueue.hpp"
#include <iostream>

typedef PriorityQueue::DataType DataType;

PriorityQueue::PriorityQueue(unsigned int capacity)
{
    heap_ = new DataType[capacity + 1];
    capacity_ = capacity;
    size_ = 0;
}
    
PriorityQueue::~PriorityQueue() 
{
    delete [] heap_;
}
    
bool PriorityQueue::enqueue(DataType val) 
{
	if(full()) return false;

	DataType temp;
	heap_[++size_] = val;

	int i = size_;
	while(i > 1 && heap_[i] > heap_[i/2])
	{
		temp = heap_[i];
		heap_[i] = heap_[i/2];
		heap_[i/2] = temp;
		
		i /= 2;
	}
	return true;
}
    
bool PriorityQueue::dequeue()
{
	if(empty()) return false;

    DataType temp = heap_[1];
    heap_[1] = heap_[size_--];

    int i = 1;
    while(2*i + 1 <= size_)
    {
    	if(heap_[2*i] > heap_[2*i + 1]) i = 2*i;
    	else i = 2*i + 1;
    	
    	temp = heap_[i];
		heap_[i] = heap_[i/2];
		heap_[i/2] = temp;
    }
    return true;
}
    
DataType PriorityQueue::max() const
{
    if(size_ == 0) return 0;
    return heap_[1];
}
    
bool PriorityQueue::empty() const
{
    return size_ == 0;
}
    
bool PriorityQueue::full() const
{
    return size_ == capacity_;
}
    
unsigned int PriorityQueue::size() const
{
    return size_;
}
    
void PriorityQueue::print() const 
{
    for(int i = 1; i <= size_; i++)
    	std::cout << heap_[i] << " ";
    std::cout << std::endl;
}