#include "BinarySearchTree.hpp"
#include <cstddef> 
#include <iostream>
#include <queue>

typedef BinarySearchTree::DataType DataType;

BinarySearchTree::Node::Node(DataType newval)
{
    this -> val = newval;
    this -> left = NULL;
    this -> right = NULL;
}

BinarySearchTree::BinarySearchTree()
{
    root_ = NULL;
    size_ = 0;
}

BinarySearchTree::~BinarySearchTree()
{
    std::queue<Node*> queue;
    queue.push(root_);

    while(!queue.empty())
    {
        if(queue.front() != NULL)
        {
            queue.push(queue.front() -> left);
            queue.push(queue.front() -> right);
            
            delete queue.front();
        }
        queue.pop();
    }
}

bool BinarySearchTree::insert(DataType val)
{
    if(root_ == NULL)
    {
        root_ = new Node(val);
        size_++;
        return true;
    }

    Node* node = root_;
    while(node -> val != val)
    {
        if(node -> val > val)
        {
            if(node -> left == NULL)
            {
                node -> left = new Node(val);
                size_++;
                return true;
            }
            node = node -> left;
        }
        else
        {
            if(node -> right == NULL)
            {
                node -> right = new Node(val);
                size_++;
                return true;
            }
            node = node -> right;
        }
    }
    return false;
}

bool BinarySearchTree::remove(DataType val)
{
    if(root_ == NULL) return false;
    else
    {
        Node *node = root_, *parent = NULL;
        while(node != NULL && node -> val != val)
        {
            parent = node;
            if(node -> val > val) node = node -> left;
            else node = node -> right;
        }

        if(node == NULL) return false;
        else if(node -> right != NULL)
        {
            if(node -> left == NULL)
            {
                if(parent != NULL)
                {
                    if(parent -> right == node) parent -> right = node -> right;
                    else parent -> left = node -> right;
                }
                else root_ = node -> right;
                delete node;
            }
            else
            {
                Node* min = node -> right;
                while(min -> left != NULL)
                {
                    min = min -> left;
                }

                DataType temp = min -> val;
                remove(temp);
                node -> val = temp;
                return true;
            }
        }
        else if(node -> left != NULL)
        {
            if(parent != NULL)
            {
                if(parent -> right == node) parent -> right = node -> left;
                else parent -> left = node -> left;
            }
            else root_ = node -> left;
            delete node;
        }
        else
        {
            if(parent != NULL)
            {
                if(parent -> right == node) parent -> right = NULL;
                else parent -> left = NULL;
            }
            else root_ = NULL;
            delete node;
        }
    }
    size_--;
    return true;
}

bool BinarySearchTree::exists(DataType val) const
{
    if(root_ == NULL) return false;

    Node* node = root_;
    while(node != NULL)
    {
        if(node -> val == val) return true;
        else if(node -> val > val) node = node -> left;
        else node = node -> right;
    }

    return false;
}

DataType BinarySearchTree::min() const
{
    Node* node = root_;
    while(node -> left != NULL)
    {
        node = node -> left;
    }
    return node -> val;
}

DataType BinarySearchTree::max() const
{
    Node* node = root_;
    while(node -> right != NULL)
    {
        node = node -> right;
    }
    return node -> val;
}

unsigned int BinarySearchTree::size() const
{
    return size_;
}

unsigned int BinarySearchTree::depth() const
{
    return getNodeDepth(root_) - 1;
}

void BinarySearchTree::print() const
{
    std::queue<Node*> queue;
    queue.push(root_);

    std::cout << "Level order: ";
    while(!queue.empty())
    {
        if(queue.front() != NULL)
        {
            queue.push(queue.front() -> left);
            queue.push(queue.front() -> right);
            
            std::cout << queue.front() -> val << " ";
        }
        queue.pop();
    }
    std::cout << std::endl;
}

int BinarySearchTree::getNodeDepth(Node* n) const
{
    if(n == NULL) return 0;

    int l = getNodeDepth(n -> left);
    int r = getNodeDepth(n -> right);

    if(l > r) return l + 1;
    else return r + 1;
}