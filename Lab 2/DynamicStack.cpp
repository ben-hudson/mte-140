#include <iostream>
#include <cstdlib>
#include "DynamicStack.hpp"

typedef DynamicStack::StackItem StackItem;  // for simplicity
const StackItem DynamicStack::EMPTY_STACK = -999;

DynamicStack::DynamicStack()
{
    items_ = (StackItem*)malloc(16*sizeof(StackItem));
    capacity_ = 16;
    size_ = 0;
    init_capacity_ = 16;
}

DynamicStack::DynamicStack(unsigned int capacity)
{
    items_ = (StackItem*)malloc(capacity*sizeof(StackItem));
    capacity_ = capacity;
    size_ = 0;
    init_capacity_ = capacity;
}

DynamicStack::~DynamicStack()
{
    free(items_);
}

bool DynamicStack::empty() const
{
    return size_ == 0;
}

int DynamicStack::size() const
{
    return size_;
}

void DynamicStack::push(StackItem value)
{
    if(size_ == capacity_)
    {
        capacity_ *= 2;
        items_ = (StackItem*)realloc(items_, capacity_*sizeof(StackItem));
    }
    
    items_[size_++] = value;
}

StackItem DynamicStack::pop()
{
    if(empty()) return EMPTY_STACK;
    
    if(--size_ <= capacity_/4 && capacity_ > init_capacity_)
    {
        capacity_ /= 2;
        items_ = (StackItem*)realloc(items_, capacity_*sizeof(StackItem));
    }
    
    return items_[size_];
}

StackItem DynamicStack::peek() const
{
    if(empty()) return EMPTY_STACK;
    
    return items_[size_ - 1];
}

void DynamicStack::print() const
{
    for(int i = 0; i < size_; i++)
        std::cout << items_[i] << " -> ";
    std::cout << "NULL\n";
}