#include <iostream>
#include "DoublyLinkedList.hpp"

DoublyLinkedList::Node::Node(DataType data)
{
	value = data;
	next = NULL;
	prev = NULL;
}

DoublyLinkedList::DoublyLinkedList()
{
	head_ = NULL;
	tail_ = NULL;
	size_ = 0;
}

DoublyLinkedList::~DoublyLinkedList()
{
	Node* end = head_;
	Node* visitor;
	while(end != NULL)
	{
		visitor = end -> next;
		delete end;
		end = visitor;
	}
}

bool DoublyLinkedList::empty() const
{
	return size_ == 0;
}

unsigned int DoublyLinkedList::size() const
{
	return size_;
}

void DoublyLinkedList::print() const
{
	Node* end = head_;
	while(end != NULL)
	{
		std::cout << end -> value << " -> ";
		end = end -> next;
	}
	std::cout << "NULL\n";
}

bool DoublyLinkedList::insert_front(DataType value)
{
	if(full())
		return false;
	else if(empty())
	{
		Node* node = new Node(value);
		head_ = node;
		tail_ = node;
		size_ = 1;
		return true;
	}
	else
	{
		Node* node = new Node(value);
		node -> next = head_;
		head_ -> prev = node;
		head_ = node;
		size_++;
		return true;
	}
}

bool DoublyLinkedList::remove_front()
{
	if(empty())
		return false;
	else if(size_ == 1)
	{
		delete head_;
		head_ = NULL;
		tail_ = NULL;
		size_ = 0;
		return true;
	}
	else
	{
		head_ = head_ -> next;
		delete head_ -> prev;
		head_ -> prev = NULL;
		size_--;
		return true;
	}
}

bool DoublyLinkedList::insert_back(DataType value)
{
	if(full())
		return false;
	else if(empty())
	{
		Node* node = new Node(value);
		head_ = node;
		tail_ = node;
		size_ = 1;
		return true;
	}
	else
	{
		Node* node = new Node(value);
		node -> prev = tail_;
		tail_ -> next = node;
		tail_ = node;
		size_++;
		return true;
	}
}

bool DoublyLinkedList::remove_back()
{
	if(empty())
		return false;
	else if(size_ == 1)
	{
		delete head_;
		head_ = NULL;
		tail_ = NULL;
		size_ = 0;
		return true;
	}
	else
	{
		tail_ = tail_ -> prev;
		delete tail_ -> next;
		tail_ -> next = NULL;
		size_--;
		return true;
	}
}

bool DoublyLinkedList::insert(DataType value, unsigned int pos)
{
	if(full() || pos > size_)
		return false;
	else if(empty() || pos == 0)
		return insert_front(value);
	else if(pos == size_)
		return insert_back(value);
	else
	{
		Node* next = getNode(pos);
		Node* prev = next -> prev;
		Node* node = new Node(value);

		node -> next = next;
		node -> prev = prev;
		prev -> next = node;
		next -> prev = node;
		size_++;
		return true;
	}
}

bool DoublyLinkedList::remove(unsigned int pos)
{
	if(empty() || pos > size_ - 1)
		return false;
	else if(size_ == 1 || pos == 0)
		return remove_front();
	else if(pos == size_ - 1)
		return remove_back();
	else
	{
		Node* node = getNode(pos);
		Node* next = node -> next;
		Node* prev = node -> prev;

		next -> prev = prev;
		prev -> next = next;
		delete node;
		size_--;
		return true;
	}
}

unsigned int DoublyLinkedList::search(DataType value) const
{
	int index = 0;
	Node* end = head_;
	while(end != NULL)
	{
		if(end -> value == value)
			return index;

		index++;
		end = end -> next;
	}
	return size_;
}

DoublyLinkedList::DataType DoublyLinkedList::select(unsigned int pos) const
{
	Node* node = getNode(pos);

	if(node != NULL)
		return node -> value;
}

bool DoublyLinkedList::replace(unsigned int pos, DataType value)
{
	if(pos > size_ - 1)
		return false;

	Node* node = getNode(pos);
	if(node != NULL)
	{
		node -> value = value;
		return true;
	}
}

DoublyLinkedList::Node* DoublyLinkedList::getNode(unsigned int pos) const
{
	if(empty())
		return NULL;

	int index = 0;
	Node* end = head_;
	while(end -> next != NULL && pos != index)
	{
		end = end -> next;
		index++;
	}
	return end;
}

bool DoublyLinkedList::full() const
{
	return size_ == CAPACITY;
}